package id.co.indivara.jdt12.universit;
import id.co.indivara.jdt12.universit.entity.Record;
import id.co.indivara.jdt12.universit.utility.mapper.MapperConvertation;
import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RecordServiceAddGradeTest {
    @Autowired
    MockMvc mockMvc;


        @Test
        public void testAddGrade() throws Exception {
            Record student = new Record();
            student.setReportId(1L);
            student.setGrade("A");
            student.setMidTest(80.0);
            student.setFinalTest(80.0);
            student.setQuis(80.0);

            mockMvc.perform(MockMvcRequestBuilders
                            .post("/api/record/")
                            .header("Authorization", getBasicAuthenticationHeader("admin", "password"))
                            .accept(MediaType.APPLICATION_JSON)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(MapperConvertation.toJson(student))
                    )
                    .andDo(print())
                    .andExpect(status().isCreated())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.reportId").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.midTest").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.quis").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.finalTest").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.grade").exists());
        }

        private String getBasicAuthenticationHeader(String username, String password) {
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(auth.getBytes());
            String headerValue = "Basic " + new String(encodedAuth);
            return headerValue;
        }
    }
