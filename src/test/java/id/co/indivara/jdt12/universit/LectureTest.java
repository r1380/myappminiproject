package id.co.indivara.jdt12.universit;
import id.co.indivara.jdt12.universit.entity.Lecture;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Base64;
import java.util.Date;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LectureTest {
    @Autowired
    private MockMvc mockMvc;

    private static final String getBasicAuthenticationHeader(String username, String password) {
    String valueToEncode = username + ":" + password;
    return "Basic " + Base64.getEncoder().encodeToString(valueToEncode.getBytes());
}

    @org.junit.Test
    public void testAutorization() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/lecture/")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", getBasicAuthenticationHeader("lecturer", "123"))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }

    @Test
    public void testLectureEntity() {
        // Create a sample lecture
        Lecture lecture = Lecture.builder()
                .lectureId(1L)
                .nama("John Doe")
                .tanggal(new Date())
                .kelamin("Male")
                .alamat("123 Main St")
                .email("john.doe@example.com")
                .nomor("1234567890")
                .build();

        // Assert the values
        Assertions.assertEquals(1L, lecture.getLectureId());
        Assertions.assertEquals("John Doe", lecture.getNama());
        Assertions.assertNotNull(lecture.getTanggal());
        Assertions.assertEquals("Male", lecture.getKelamin());
        Assertions.assertEquals("123 Main St", lecture.getAlamat());
        Assertions.assertEquals("john.doe@example.com", lecture.getEmail());
        Assertions.assertEquals("1234567890", lecture.getNomor());
    }


}
