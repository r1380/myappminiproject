package id.co.indivara.jdt12.universit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        LectureServiceTest.class,
        RecordServiceAddGradeTest.class,
        StudentControllerTest.class,
        SubjectServiceTest.class,
        SubjectServiceTest.class,
        GradeTest.class,
        StudentTest.class,
        LectureTest.class,
        SemesterTest.class,
        RecordTest.class,
        SubjectTest.class
})
public class SuiteTest {
}
