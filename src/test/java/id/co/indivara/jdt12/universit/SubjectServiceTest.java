package id.co.indivara.jdt12.universit;

import id.co.indivara.jdt12.universit.entity.Subject;
import id.co.indivara.jdt12.universit.service.SubjectService;
import id.co.indivara.jdt12.universit.utility.mapper.MapperConvertation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Base64;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SubjectServiceTest {
    @Autowired
    SubjectService service;
    @Autowired
    MockMvc mockMvc;

    private static final String getBasicAuthenticationHeader(String username, String password) {
        String valueToEncode = username + ":" + password;
        return "Basic " + Base64.getEncoder().encodeToString(valueToEncode.getBytes());
    }
    @Test
    public void testAddSubject() throws Exception {
        Subject student = new Subject();
        student.setSubjectId(1L);
        student.setSubjectName("Arab");
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/subject/")
                        .header("Authorization", getBasicAuthenticationHeader("admin", "password"))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertation.toJson(student))
                )
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.subjectName").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.subjectId").exists());

    }

}
