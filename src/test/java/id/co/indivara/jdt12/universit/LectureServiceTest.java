package id.co.indivara.jdt12.universit;

import id.co.indivara.jdt12.universit.entity.Lecture;
import id.co.indivara.jdt12.universit.service.LectureService;
import id.co.indivara.jdt12.universit.utility.mapper.MapperConvertation;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Base64;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LectureServiceTest {
    @Autowired
    MockMvc mockMvc;
    @Autowired
    private LectureService lectureService;
    private static final String getBasicAuthenticationHeader(String username, String password) {
        String valueToEncode = username + ":" + password;
        return "Basic " + Base64.getEncoder().encodeToString(valueToEncode.getBytes());
    }

    @Test
    public void testAutorization() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/lecture/")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", getBasicAuthenticationHeader("lecturer", "123"))
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }
    @Test
    public void testAllLectureTest() throws Exception {
        List<Lecture> lecturecheck = lectureService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/lecture/all/lecture")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", getBasicAuthenticationHeader("lecturer", "123"))
                )
                .andDo(result -> {
                    List<Lecture> lectures = MapperConvertation.getAllData(result.getResponse().getContentAsString(), Lecture.class);
                    Assertions.assertNotNull(lectures);
                    Assertions.assertEquals(lecturecheck.get(0).getNama(), lectures.get(0).getNama());
                })
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].lectureId").isNotEmpty());


    }
    @Test
    public void testcrudCreate() throws Exception {
        List<Lecture> lecturecheck = lectureService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/lecture/")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", getBasicAuthenticationHeader("lecturer", "123"))
                )
                .andDo(result -> {
                    List<Lecture> lectures = MapperConvertation.getAllData(result.getResponse().getContentAsString(), Lecture.class);
                    Assertions.assertNotNull(lectures);
                    Assertions.assertEquals(lecturecheck.get(0).getEmail(), lectures.get(0).getEmail());
                })
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].email").isNotEmpty());

    }
    @Test
    public void testcrudDelete() throws Exception {
        List<Lecture> lecturecheck = lectureService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/lecture/")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", getBasicAuthenticationHeader("lecturer", "123"))
                )
                .andDo(result -> {
                    List<Lecture> lectures = MapperConvertation.getAllData(result.getResponse().getContentAsString(), Lecture.class);
                    Assertions.assertNotNull(lectures);
                    Assertions.assertEquals(lecturecheck.get(0).getNomor(), lectures.get(0).getNomor());
                })
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].nomor").isNotEmpty());

    }
    @Test
    public void testcrudUpdate() throws Exception {
        List<Lecture> lecturecheck = lectureService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/lecture/")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", getBasicAuthenticationHeader("lecturer", "123"))
                )
                .andDo(result -> {
                    List<Lecture> lectures = MapperConvertation.getAllData(result.getResponse().getContentAsString(), Lecture.class);
                    Assertions.assertNotNull(lectures);
                    Assertions.assertEquals(lecturecheck.get(0).getKelamin(), lectures.get(0).getKelamin());
                })
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].kelamin").isNotEmpty());

    }
    @Test
    public void testcrudInsert() throws Exception {
        List<Lecture> lecturecheck = lectureService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/lecture/")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", getBasicAuthenticationHeader("lecturer", "123"))
                )
                .andDo(result -> {
                    List<Lecture> lectures = MapperConvertation.getAllData(result.getResponse().getContentAsString(), Lecture.class);
                    Assertions.assertNotNull(lectures);
                    Assertions.assertEquals(lecturecheck.get(0).getAlamat(), lectures.get(0).getAlamat());
                })
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].alamat").isNotEmpty());

    }


}
