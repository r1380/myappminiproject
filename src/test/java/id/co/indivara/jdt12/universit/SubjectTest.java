package id.co.indivara.jdt12.universit;
import id.co.indivara.jdt12.universit.entity.Record;
import id.co.indivara.jdt12.universit.entity.Semester;
import id.co.indivara.jdt12.universit.entity.Subject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SubjectTest {
    @Autowired
    private MockMvc mockMvc;

    private static final String getBasicAuthenticationHeader(String username, String password) {
        String valueToEncode = username + ":" + password;
        return "Basic " + Base64.getEncoder().encodeToString(valueToEncode.getBytes());
    }

    @org.junit.Test
    public void testAutorization() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/lecture/")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", getBasicAuthenticationHeader("lecturer", "123"))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }
    @Test
public void testSubjectEntity() {

    Record record = Record.builder()
            .reportId(1L)
            .grade("B")
            .build();


    List<Record> recordList = new ArrayList<>();
    recordList.add(record);


    List<Semester> semesterList = new ArrayList<>();


    Subject subject = Subject.builder()
            .subjectId(1L)
            .subjectName("Mathematics")
            .records(recordList)
            .semesters(semesterList)
            .build();

    // Assert the values
    Assertions.assertEquals(1L, subject.getSubjectId());
    Assertions.assertEquals("Mathematics", subject.getSubjectName());
    Assertions.assertEquals(recordList, subject.getRecords());
    Assertions.assertEquals(semesterList, subject.getSemesters());
}
}
