package id.co.indivara.jdt12.universit;
import id.co.indivara.jdt12.universit.entity.Lecture;
import id.co.indivara.jdt12.universit.entity.Semester;
import id.co.indivara.jdt12.universit.entity.Subject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Base64;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SemesterTest {
    @Autowired
    private MockMvc mockMvc;

    private static final String getBasicAuthenticationHeader(String username, String password) {
        String valueToEncode = username + ":" + password;
        return "Basic " + Base64.getEncoder().encodeToString(valueToEncode.getBytes());
    }

    @org.junit.Test
    public void testAutorization() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/lecture/")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", getBasicAuthenticationHeader("lecturer", "123"))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }
    @Test
    public void testSemesterEntity() {
        // Create  lecture
        Lecture lecture = Lecture.builder()
                .lectureId(1L)
                .nama("John Doe")
                .build();

        // Create subject
        Subject subject = Subject.builder()
                .subjectId(1L)
                .subjectName("Math")
                .build();

        // Create  semester
        Semester semester = Semester.builder()
                .id(1L)
                .lecture(lecture)
                .subject(subject)
                .semester("Spring 2023")
                .build();


        Assertions.assertEquals(1L, semester.getId());
        Assertions.assertEquals(lecture, semester.getLecture());
        Assertions.assertEquals(subject, semester.getSubject());
        Assertions.assertEquals("Spring 2023", semester.getSemester());
    }

}
