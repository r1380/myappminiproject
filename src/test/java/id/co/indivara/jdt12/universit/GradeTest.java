package id.co.indivara.jdt12.universit;

import id.co.indivara.jdt12.universit.controller.RecordController;
import id.co.indivara.jdt12.universit.service.LectureService;
import id.co.indivara.jdt12.universit.service.RecordService;
import id.co.indivara.jdt12.universit.utility.mapper.MapperConvertation;
import lombok.var;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Base64;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static sun.misc.Version.print;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GradeTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private LectureService lectureService;
    private static final String getBasicAuthenticationHeader(String username, String password) {
        String valueToEncode = username + ":" + password;
        return "Basic " + Base64.getEncoder().encodeToString(valueToEncode.getBytes());
    }

    @Test
    public void testAutorization() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/lecture/")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", getBasicAuthenticationHeader("lecturer", "123"))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }
}
