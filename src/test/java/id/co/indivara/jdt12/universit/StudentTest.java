package id.co.indivara.jdt12.universit;

import id.co.indivara.jdt12.universit.entity.Record;
import id.co.indivara.jdt12.universit.entity.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StudentTest {
    @Autowired
    private MockMvc mockMvc;

    private static final String getBasicAuthenticationHeader(String username, String password) {
        String valueToEncode = username + ":" + password;
        return "Basic " + Base64.getEncoder().encodeToString(valueToEncode.getBytes());
    }

    @org.junit.Test
    public void testAutorization() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/lecture/")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", getBasicAuthenticationHeader("lecturer", "123"))
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }
    @Test
    public void testStudentEntity() {

        Record record = Record.builder()
                .reportId(1L)
                .grade("A")
                .build();


        List<Record> recordList = new ArrayList<>();
        recordList.add(record);

        Student student = Student.builder()
                .studentId(1L)
                .nama("John Doe")
                .tanggal(new Date())
                .kelamin("Male")
                .alamat("123 Main St")
                .email("johndoe@example.com")
                .nomor("1234567890")
                .recordList(recordList)
                .build();

        Assertions.assertEquals(1L, student.getStudentId());
        Assertions.assertEquals("John Doe", student.getNama());
        Assertions.assertNotNull(student.getTanggal());
        Assertions.assertEquals("Male", student.getKelamin());
        Assertions.assertEquals("123 Main St", student.getAlamat());
        Assertions.assertEquals("johndoe@example.com", student.getEmail());
        Assertions.assertEquals("1234567890", student.getNomor());
        Assertions.assertEquals(recordList, student.getRecordList());
    }
}
