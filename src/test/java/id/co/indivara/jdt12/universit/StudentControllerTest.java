package id.co.indivara.jdt12.universit;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import id.co.indivara.jdt12.universit.entity.Student;
import id.co.indivara.jdt12.universit.service.StudentService;
import id.co.indivara.jdt12.universit.utility.mapper.MapperConvertation;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDate;
import java.util.*;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    private StudentService studentService;

    private static final String getBasicAuthenticationHeader(String username, String password) {
        String valueToEncode = username + ":" + password;
        return "Basic " + Base64.getEncoder().encodeToString(valueToEncode.getBytes());
    }



    @Test
    public void addStudent() throws Exception {
        Student student=new Student();
        student.setStudentId(1L);
        student.setAlamat("Garut");
        student.setEmail("1");
        student.setNama("riki");
        student.setNomor("087");
        student.getRecordList();
        student.setTanggal(new Date(2023,3,29));
        student.setKelamin("pria");
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/student/add/")
                        .header("Authorization", getBasicAuthenticationHeader("admin", "password"))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertation.toJson(student))
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.studentId").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nama").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nomor").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.kelamin").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.tanggal").exists());

    }


}