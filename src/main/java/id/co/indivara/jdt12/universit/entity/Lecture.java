package id.co.indivara.jdt12.universit.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.co.indivara.jdt12.universit.controller.GenericController;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "lecturers")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
//@Accessors(chain = true)
public class Lecture  {

    @Id
    @Column(name = "lecture_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long lectureId;
    @Column(name = "nama")
    private String nama;
    @Column(name = "tanggal_lahir")
    private Date tanggal;
    @Column(name = "jenis_kelamin")
    private String kelamin;
    @Column(name = "alamat")
    private String alamat;
    @Column(name = "email")
    private String email;
    @Column(name = "nomor_telepon")
    private String nomor;
    @OneToMany(mappedBy = "lecture")
    private List<Semester> semesters;

    @JoinColumn(name = "subject_id",insertable = false,updatable = false)
    @OneToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Subject subject;



}
