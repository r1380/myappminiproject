package id.co.indivara.jdt12.universit.controller;

import id.co.indivara.jdt12.universit.entity.Lecture;
import id.co.indivara.jdt12.universit.entity.Student;
import id.co.indivara.jdt12.universit.exceptions.ResourceNotFoundException;
import id.co.indivara.jdt12.universit.repo.LectureRepository;
import id.co.indivara.jdt12.universit.repo.ResponMessage;
import id.co.indivara.jdt12.universit.repo.StudentRepository;
import id.co.indivara.jdt12.universit.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("/api/lecture/")
public class LectureController extends GenericController<Lecture> {
    @Autowired
    private LectureRepository lectureRepository;


        @GetMapping("/findlecture/{lectureId}")
    public Lecture findLecture(@PathVariable Long lectureId) throws Exception{
        Optional<Lecture> lecture = lectureRepository.findById(lectureId);
        Lecture lect = lecture.orElseThrow(() -> new RuntimeException("Lecture not found"));
        return lect;
    }


    @PostMapping("/add/lecture/")
    public ResponMessage add(@RequestBody Lecture lecture){
        lectureRepository.save(lecture);
        return new ResponMessage("Data updated successfully"+  lecture+ " ", 200);
    }
    @GetMapping("/all/lecture")
    public ArrayList<Lecture> findAllStudent() {
        ArrayList<Lecture> lectures = (ArrayList<Lecture>) lectureRepository.findAll();
        return lectures;

    }


}
