package id.co.indivara.jdt12.universit.controller;

import id.co.indivara.jdt12.universit.entity.Lecture;
import id.co.indivara.jdt12.universit.entity.Student;
import id.co.indivara.jdt12.universit.exceptions.ResourceNotFoundException;
import id.co.indivara.jdt12.universit.repo.StudentRepository;
import id.co.indivara.jdt12.universit.service.GenericService;
import id.co.indivara.jdt12.universit.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/student/")
public class StudentController extends GenericController<Student> {
    @Autowired
    StudentService studentService;

    @Autowired
    StudentRepository studentRepository;


    @PostMapping("/add")
    public Student addStudent(@RequestBody Student student){
        return studentService.createStudent(student);
    }

    @GetMapping("/all/student")
    public ArrayList<Student> findAll() {
        ArrayList<Student>students = (ArrayList<Student>) studentService.findAllStudent();
        return students;

    }


}
