package id.co.indivara.jdt12.universit.repo;

import id.co.indivara.jdt12.universit.entity.Semester;
import id.co.indivara.jdt12.universit.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SemesterRepository extends GenericRepository <Semester> {
//    void deleteSemester(Long id);
//    Semester updateSemester (Semester semester, Long id);
//    List<Semester> semesterList();
}
