package id.co.indivara.jdt12.universit.controller;

import id.co.indivara.jdt12.universit.entity.Record;
import id.co.indivara.jdt12.universit.repo.RecordRepository;
import id.co.indivara.jdt12.universit.repo.ResponMessage;
import id.co.indivara.jdt12.universit.service.GenericService;
import id.co.indivara.jdt12.universit.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/record/")
public class RecordController extends GenericController<Record> {
    @Autowired
    RecordService recordService;

    @PostMapping("add/grade")
    public Record createRecord(@RequestBody Record record)throws Exception{
        return recordService.createRecord(record);
    }
    @PostMapping("update/grade")
    public Record updateRecordStudent(@RequestBody Record record)throws Exception{
        return recordService.updateRecordStudent(record);
    }

}
