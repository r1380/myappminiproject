package id.co.indivara.jdt12.universit.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "subject")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subject_id")
    private Long subjectId;
    @Column(name = "subject_name")
    private String subjectName;
    @OneToMany(mappedBy = "subject")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<Record> records;
    @OneToMany(mappedBy = "subject")
    private List<Semester> semesters;
}
