package id.co.indivara.jdt12.universit.service;

import id.co.indivara.jdt12.universit.entity.Lecture;
import id.co.indivara.jdt12.universit.entity.Semester;
import id.co.indivara.jdt12.universit.entity.Subject;
import id.co.indivara.jdt12.universit.repo.SemesterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class SemesterService extends GenericService<Semester> {
    @Autowired
    SemesterRepository semesterRepository;
    @Autowired
    LectureService lectureService;
    @Autowired
    SubjectService subjectService;

    public Semester saveSemester(Semester semester) {
        System.out.println(semester.getLecture());
        System.out.println(semester.getSubject());
        Lecture lecture = lectureService.findById(semester.getLecture().getLectureId());
        Subject subject = subjectService.findById(semester.getSubject().getSubjectId());
        semester.setLecture(lecture);
        semester.setSubject(subject);
        System.out.println(semester);
        return semesterRepository.save(semester);
    }

}
