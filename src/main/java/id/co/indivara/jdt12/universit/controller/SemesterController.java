package id.co.indivara.jdt12.universit.controller;

import id.co.indivara.jdt12.universit.entity.Semester;
import id.co.indivara.jdt12.universit.entity.Student;
import id.co.indivara.jdt12.universit.service.GenericService;
import id.co.indivara.jdt12.universit.service.SemesterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/api/semester/")
public class SemesterController extends GenericController<Semester> {
    @Autowired
    SemesterService semesterService;

    @PostMapping("/add/semester")
    public Semester addSemester(@RequestBody Semester semester){
        return semesterService.saveSemester(semester);
    }



}
