package id.co.indivara.jdt12.universit.controller;

import id.co.indivara.jdt12.universit.entity.Subject;
import id.co.indivara.jdt12.universit.service.GenericService;
import id.co.indivara.jdt12.universit.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/subject/")
public class SubjectController extends GenericController<Subject> {
    @Autowired
    SubjectService subjectService;
    @PostMapping("/add")
    public Subject createMatkul(@RequestBody Subject subject){
        return subjectService.createMatkul(subject);
    }
    @GetMapping("/subject/all")
    public List<Subject> getAllSubject(){
        return subjectService.getAllSubject();
    }
}
