package id.co.indivara.jdt12.universit.repo;

import id.co.indivara.jdt12.universit.entity.Lecture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LectureRepository extends GenericRepository<Lecture> {


    @Override
    Optional<Lecture> findById(Long aLong);
}
