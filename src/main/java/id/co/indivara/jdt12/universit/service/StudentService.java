package id.co.indivara.jdt12.universit.service;

import id.co.indivara.jdt12.universit.entity.Record;
import id.co.indivara.jdt12.universit.entity.Student;
import id.co.indivara.jdt12.universit.exceptions.ResourceNotFoundException;
import id.co.indivara.jdt12.universit.repo.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public class StudentService extends GenericService <Student>  {

    @Autowired
    StudentRepository studentRepository;
    public Student createStudent(Student student){
        return studentRepository.save(student);
    }
    public List<Student> findAllStudent(){
        return studentRepository.findAll();
    }

//    public Student updateDepartment(Student department, Long studentId) {
//        return null;
//    }

    //Delete : /student/{id}
    public void deleteStudent(Long studentId) {

    }


//        public Optional<Student> findById(Long studentId)throws ResourceNotFoundException {
//            return Optional.ofNullable(studentRepository.findById(studentId).orElseThrow(()->new ResourceNotFoundException("Student id "+studentId+"not found")));
//        }
}
