package id.co.indivara.jdt12.universit.service;

import id.co.indivara.jdt12.universit.entity.Lecture;
import id.co.indivara.jdt12.universit.entity.Student;
import id.co.indivara.jdt12.universit.repo.LectureRepository;
import id.co.indivara.jdt12.universit.repo.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class LectureService extends GenericService<Lecture>{
    @Autowired
    LectureRepository lectureRepository;
    public Lecture createLecture(Lecture lecture){
        return lectureRepository.save(lecture);
    }
    public List<Lecture> findAllStudent(){
        return lectureRepository.findAll();
    }
    public Lecture findById(Long id) {
        return lectureRepository.findById(id).orElseThrow(()-> new RuntimeException("id tidak ditemukkan"));
    }

}
