package id.co.indivara.jdt12.universit.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "semester")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Semester {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ToString.Exclude
    @JsonProperty(access= JsonProperty.Access.WRITE_ONLY)
    @ManyToOne
    @JoinColumn(name = "lecture_id")
    private Lecture lecture;
    @ToString.Exclude
    @JsonProperty(access= JsonProperty.Access.WRITE_ONLY)
    @ManyToOne
    @JoinColumn(name = "subject_id")
    private Subject subject;

    @Column(name = "semester")
    private String semester;

}
